import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

export interface EmpoyeeData {
  EmployeeID: number;
  FIO?: string;
  EventDescription?: string;
  EventDescriptionID?: number;
  Date?: any;
}


@Injectable({providedIn: 'root'})



export class ApiService {

  private employeesData = 'http://localhost:3000/api/v1/employees';
  private employeeSummaryTime = 'http://localhost:3000/api/v1/employees/:employeeId/summary/:date';

  private summaryTime;

  public employeesList: EmpoyeeData[] = [];
  public currentEmloyee;

  constructor(private http: HttpClient) {}

  fetchEmployees(): Observable<EmpoyeeData[]> {
    return this.http.get<EmpoyeeData[]>(this.employeesData)
    .pipe(tap( employeesList => this.employeesList = employeesList));
  }

  getEmployeeSummaryTime(id, currentDate) {
    return this.http.get(this.employeeSummaryTime.replace(':employeeId', id).replace(':date', currentDate))
      .pipe(tap(result => this.summaryTime = result));
  }

}
