import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import { ActivatedRoute } from '@angular/router';
import {DatePipe} from '@angular/common';
import {delay} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-employee-time',
  templateUrl: './employee-time.component.html',
  styleUrls: ['./employee-time.component.scss']
})
export class EmployeeTimeComponent implements OnInit {

  private userid: string;
  private timePipe = new DatePipe('en-US');
  private workingTime: any;
  selectedDate = new FormControl((new Date()).toISOString());


  constructor(private route: ActivatedRoute, private apiService: ApiService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.userid = params.get('userid');
      this.apiService.currentEmloyee = params.get(('userid'));
    });
    this.setCurrentEmployee();
    this.setSelectedDate();
    this.getSummaryTime(this.selectedDate.value);
  }

  setCurrentEmployee() {
    this.apiService.currentEmloyee = this.apiService.employeesList.filter((item) => {
      // tslint:disable-next-line:radix
      return item.EmployeeID === parseInt(this.userid);
    })[0];
  }

  setSelectedDate() {
    this.selectedDate = new FormControl(this.timePipe.transform(this.apiService.currentEmloyee.Date, 'yyyy-MM-dd'));
  }

  getSummaryTime(date) {
    date = this.timePipe.transform(date, 'yyyy-MM-dd');
    this.workingTime = null;
    this.apiService.getEmployeeSummaryTime(this.apiService.currentEmloyee.EmployeeID, date)
      .pipe(delay(800)).subscribe((result) => {
      this.workingTime = result.totalTime;
    });
  }

}
