import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import {MatTableDataSource} from '@angular/material';
import {delay, repeat} from 'rxjs/operators';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.scss']
})
export class EmployeesListComponent implements OnInit {

  private loading = true;

  constructor(private apiService: ApiService) { }

  private dataSource;

  displayedColumns: string[] = ['EmployeeID', 'FIO', 'EventDescriptionID', 'Date'];

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.apiService.fetchEmployees().pipe(delay(500)).subscribe((result) => {
        this.loading = false;
        this.dataSource = new MatTableDataSource(result);
    });
  }

}
